#!/usr/bin/env python

import os, sys
import argparse
import itkdb
import random

from database import pwbdb
from database import pwbdbtools
from database import pwbdbpandas
from database import transitions

def loadwafer(client, panelid, position, wafer, waferpos):
    """
    Associate an AMAC to the Powerboard.
    
    Parameters:
     client (itkdb.Client): Authenticated ITk DB client
     panelid (str): Panel ID to load (VBBXXXX)
     position (int): Powerboard position
     wafer (str): Wafer ID
     waferpos (int): Chip position on wafer
    """

    #
    # Find panel component
    panel = transitions.Panel(panelid, client=client) 

    # Find the necessary AMAC

    chips=pwbdbpandas.listComponentsByProperty(client, 'S',
                                               componentType='AMAC',
                                               propertyFilter=[
                                                   {'code':'WAFER_NAME','operator':'=','value':wafer   },
                                                   {'code':'DIE_INDEX' ,'operator':'=','value':waferpos},
                                               ])

    ## Check whether something was found
    if len(chips.index)!=1:
        raise Exception('Returned non-1 AMACv2\'s with the corresponding wafer identification.')

    ## Check property filter
    chip=chips[(chips.WAFER_NAME==wafer)&(chips.DIE_INDEX==waferpos)]
    if len(chip.index)==0:
        raise Exception('Unable to find an AMACv2 with the corresponding wafer identification.')

    chip=pwbdb.Component(chip.iloc[0].code, client=client)
    if len(chip.parents)>0:
        raise Exception('Found AMACv2 ({}) already has a parent!'.format(chip.code))

    print('Found an AMACv2: {}'.format(chip.code))    

    # Assemble :)
    pwb=panel.children['PWB'][position]
    if len(pwb.children['AMAC'])>0:
        raise Exception('Powerboard ({}) already has an AMACv2!'.format(pwb.code))

    print('Assembling to Powerboard {}'.format(pwb.code))
    pwb.assemble(chip)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Manually load a Powerboard.")

    parser.add_argument("panelid"           , help="Panel identifier (VBBXXXX).")
    parser.add_argument("position", type=int, help="Powerboard position.")
    parser.add_argument("wafer"   , type=str, help="AMAC wafer ID.")
    parser.add_argument("waferpos", type=int, help="AMAC wafer position.")

    args = parser.parse_args()

    c = pwbdbtools.get_db_client()

    loadwafer(c, args.panelid, args.position, args.wafer, args.waferpos)
