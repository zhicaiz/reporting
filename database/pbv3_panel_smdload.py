#!/usr/bin/env python

import os, sys
import argparse
import itkdb
import random

from database import pwbdb
from database import pwbdbtools
from database import transitions

def make_pwb_parent(flex):
    """
    Create a Powerboard component for `flex` and assemble it with the
    corresponing flex. The version and batch are set to the same value
    as the parent flex array. The number is set to a unique number in
    the [9000,10000) range.

    Parameters:
      flex (Component): Flex component who's parent to create

    Return:
     component: New Powerboard component
    """

    # Check for existing parent
    try:
        return next(filter(lambda p: p.componentType=='PWB', flex.parents))
    except StopIteration:
        pass
    print('Creating Powerboard...')
    # Get existing PB_NUMBWE
    pfilt=[
        {
            'code': 'PB_NUMBWE',
            'operator': '>=',
            'value': 9000
        }
        ]
    r=flex.client.get('listComponentsByProperty', json={'project':'S', 'componentType':'PWB', 'propertyFilter':pfilt})
    existing=set([pwbdbtools.get_property(pb['properties'], 'PB_NUMBWE') for pb in r])

    # Get unique number
    bogus=None
    while not bogus:
        bogus=random.randint(9000,10000)
        if bogus in existing:
            bugus=None

    # Get parent version/batch
    flexarray=next(filter(lambda p: p.componentType=='PB_FLEX_ARRAY', flex.parents))
    version=flexarray.property('VERSION')
    batch  =flexarray.property('BATCH')

    # Create parent!
    newpwb={
        'project':'S',
        'subproject':'SB',
        'institution':'LBNL_STRIP_POWERBOARDS',
        'componentType':'PWB',
        'type':'B3',
        'serialNumber': pwbdbtools.create_serial(version, batch, bogus),
        'properties':{
            'VERSION': version,
            'BATCH': batch,
            'PB_NUMBWE': bogus
            }
        }
    r=flex.client.post('registerComponent',json=newpwb)
    powerboard=pwbdb.Component(r['component']['code'], client=flex.client)
    print('\t{}'.format(pwbdbtools.create_serial(version,batch,bogus)))

    # Assemble to make it official
    print('Assemble flex')
    powerboard.assemble(flex)
    print('\tOK')

    return powerboard

def smdload(flex, bpol_type='BPOL12V4', linpol_type='LINPOL12V', currentLocation = 'LBNL_STRIP_POWERBOARDS'):
    """
    SMD load a flex.
    
    1. Create parent Powerboard component with bogus PB_NUMBWE
    2. Attach flex to Powerboard.
    3. Attach random linPOL and bPOL to Powerboard.

    Parameters:
     flex (Component): Flex which should be assembled into a Powerboard
     bpol_type (str): bPOL subtype to add
     linpol_type (str): linPOL subtype to add
     currentLocation (str): filter for currentLocation of random components
    """
    # Check if flex has a parent, otherwise make a new component
    try:
        powerboard=next(filter(lambda p: p.componentType=='PWB', flex.parents))
    except StopIteration:
        powerboard=make_pwb_parent(flex)

    #
    # Assemble chips

    # linPOL
    if 'PWB_LINPOL' not in powerboard.children:
        print('Assemble linPOL')
        linPOL=pwbdb.Component(pwbdbtools.get_random_components(flex.client, 'SG', 'PWB_LINPOL', linpol_type, currentLocation)[0], client=flex.client)
        if linPOL==None:
            print('\tUnable to find free linPOLs of type {}...'.format(linpol_type))
        else:
            powerboard.assemble(linPOL)
            print('\tOK')

    # bPOL
    if 'BPOL12V' not in powerboard.children:
        bPOL=pwbdb.Component(pwbdbtools.get_random_components(flex.client, 'SG', 'BPOL12V', bpol_type, currentLocation)[0], client=flex.client)
        if linPOL==None:
            print('\tUnable to find free bPOLs of type {}...'.format(bpol_type))
        else:
            print('Assemble bPOL')
            powerboard.assemble(bPOL)
            print('\tOK')

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="SMD load a Powerboard.")

    parser.add_argument("panelid" , help="Panel identifier (VBBXXXX).")
    parser.add_argument("-b","--bpol"     , default='BPOL12V4', help="Assemble bPOL12V of given type.")
    parser.add_argument("-l","--linpol"   , default='LINPOL12V', help="Assemble linPOL12V of given type.")    
    parser.add_argument("-c","--currentLocation", default = 'LBNL_STRIP_POWERBOARDS', help="Filter random POLs by current location.")

    args = parser.parse_args()

    c = pwbdbtools.get_db_client()

    #
    # Find panel component
    panel = transitions.Panel(args.panelid, c)    

    # Loop over children
    for child in panel.children['PB_FLEX'].values():
        smdload(child, bpol_type=args.bpol, linpol_type=args.linpol, currentLocation=args.currentLocation)
    
