from collections import defaultdict

from database import pwbdbtools

class Component:
    """
    Representation of a single component in the database.

    Information is loaded on-demand from the database. Hence a
    valid and authenticated client is required. However after 
    queries once, component properties are cached locally.
    """

    def __init__(self, code, client):
        """
        Load component according to code. Code can be anything that is an
        argument to the getComponent call (ie: DB code or serial number).

        Parameters:
         code (str): Component code or serial number
         client (itkdb.Client): Authenticated ITKDB client for performing queries
        """

        self.code=code
        self.client=client

        # Data dump
        self._dump=None

        # Cached results.
        self._children=None
        self._parents=None

    def invalidate(self):
        """
        Invalidate any cached properties. This will cause the data to be
        fetched again from database as needed.
        """
        self._dump    =None
        self._children=None
        self._parents =None

    @property
    def dump(self):
        """
        Data dump from getComponent. Not ment for outside consumption.
        """
        if self._dump==None:
            self._dump=self.client.get('getComponent',json={'component':self.code})
        return self._dump

    @property
    def children(self):
        """
        Component children stored as a dictionary.

        The key is the child's componentType.

        The value is a dictionary with 
          key: order
          value: `Component` object corresponding to the child.
        """
        if self._children==None:
            # Lookup
            self._children=defaultdict(dict)
            for c in filter(lambda c: c['component'], self.dump['children']):
                self._children[c['componentType']['code']][c['order']]=Component(c['component']['code'], client=self.client)

        return self._children        

    @property
    def parents(self):
        """
        Component parents as a list of `Component` objects.
        """
        if self._parents==None:
            self._parents=[Component(p['component']['code'], client=self.client) for p in self.dump['parents']]

        return self._parents

    @property
    def serialNumber(self):
        return self.dump['serialNumber']
    
    @property
    def currentStage(self):
        return self.dump['currentStage']['code']

    @property
    def componentType(self):
        return self.dump['componentType']['code']

    @currentStage.setter
    def currentStage(self, stage):
        self.client.post('setComponentStage',json={'component':self.code, 'stage':stage})
        self._currentStage=stage

    def property(self, code, newvalue=None):
        """
        Return or set value of property code.
        """
        if newvalue!=None:
            self.client.post('setComponentProperty', json={'component':self.code, 'code':code, 'value':newvalue})
            self.invalidate()
        else:
            return pwbdbtools.get_property(self.dump['properties'], code)

    def assemble(self, child, slot=None):
        """
        Assemble `child` onto `self` component.

        Parameters:
         child (Component): New Child
         slot (int): Slot to which component should be assigned. None if first available.
        """
        if slot==None:
            self.client.post('assembleComponent', json={'parent':self.code, 'child':child.code})
        else:
            # Get mongo ID for slot index
            slot=next(filter(lambda c: c['order']==slot and c['componentType']['code']==child.componentType, self.dump['children']))['id']
            
            self.client.post('assembleComponentBySlot', json={'parent':self.code, 'child':child.code, 'slot':slot})
        self.invalidate()

    def disassemble(self, child):
        """
        Disassemble `child` form `self` component.

        Parameters:
         child (Component): Child to remove
        """
        self.client.post('disassembleComponent', json={'parent':self.code, 'child':child.code})
        self.invalidate()
