#!/usr/bin/env python

import os, sys
import argparse
import itkdb

from database import pwbdbtools
from database import transitions

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Transition to the next stage for a Panel.")

    parser.add_argument("panelid"      , help="Panel identifier (VBBXXXX).")
    parser.add_argument("stage"        , help="Code for next stage.")
    parser.add_argument("-f","--force" , action='store_true',
                        help="Force transition without consistency check.")
    parser.add_argument("-p","--pretend", action='store_true',
                        help="Perform consistency check only. Do not transition.")

    args = parser.parse_args()

    if args.stage not in transitions.available:
        print('Transition to {} not available'.format(args.stage))
        print('Available:')
        for stage in transition.available:
            print('\t{}'.format(stage))
        sys.exit(1)
    transition=transitions.available[args.stage]

    c = pwbdbtools.get_db_client()

    #
    # Find panel component and get current stage
    panel = transitions.Panel(args.panelid, c)

    print('Transition {} of type {}'.format(args.panelid, panel.componentType))
    print('\tCurrent Stage: {}'.format(panel.currentStage))

    #
    # Check whether transition is possible
    if not args.force:
        print('Checking if transition is allowed...')
        if not transition.check_panel(panel):
            sys.exit(1)
        print('\tOK')

    #
    # Perform the transition
    if not args.pretend:
        transition.perform(panel)
