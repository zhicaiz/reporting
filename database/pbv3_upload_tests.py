#!/usr/bin/env python

import os, sys
import argparse
import json

from database import pwbdbtools

def read_test_result_file(fname, institution=None):
    """
    Read a test result file and make some common modifications
    to the common structure for database compability.

    Checks the following:
     1. Presence of `config` field
     2. Presence of `config/componenet` (serial number)
     3. Presence of `runNumber`
     4. presence of `tests` array

    Decorate each test with the following:
     `runNumber`: Top level run number
     `institution`: Institute code (from config or argument)
     `properties`: Set field to empty dictionary if it does not exist
     `properties/CONFIG`: `config/runNumber` from top-level

    Parameters:
     fname (string): Path to results file
     institution (string): Instituion to upload tests under (None to use value from file)

    Returns:
     dict: File contents as a dictionary, None on error
    """

    d = open(fname).read()
    data = json.loads(d)

    # Validate inputs
    if "config" not in data:
        print("Missing the AMACv2 configuration used during testing")
        return None

    if "component" not in data["config"]:
        print("Missing component name (via config)")
        return None

    if "runNumber" not in data:
        print("Missing runNumber")
        return None

    if "tests" not in data:
        print("Missing test results")
        return None



    #
    # Copy common meta-data to the test results

    # Update institution in the config
    if institution!=None:
        data['config']['institution']=institution

    # Test results
    for test in data.get("tests",[]):
        test["runNumber"]= data["runNumber"]
        test["institution"]= data['config']['institution']
        test["component"]= data["config"]["component"]
        if 'properties' not in test: test["properties"]={}
        if 'runNumber' in data['config']:
            test["properties"]["CONFIG"]=data["config"]["runNumber"]

    return data

def parse_serial(snum):
    """
    Parse PBv3 serial to extract the version and other
    itentifying properties.

    20USBP0VBBXXXX

    V: version
    BB: batch
    XXXX: powerboard number

    Return: tuple (version, batch, number, subTypeCode)
    """
    version = int(snum[7])
    bnum = int(snum[8:10])
    pnum = int(snum[10:14])

    if version == 0:
        subType = "B2"
    elif version in [1,2,3]:
        subType = "B3"

    return version, bnum, pnum, subType

def upload_test_result_file(path, client, institution=None):
    """
    Upload all results stored in a test file to the ITk
    production database.

    If a component with the same serial number is not register, then
    a new component is registered in the ITk PD.

    Parameters:
    path (string): Path to the results file.
    client (itkdb.Client): Authenticated ITKDB client to use for the upload
    institution (string): Institute to upload results under (set to None if to use input value)

    Returns:
    bool: True on success, False otherwise    
    """

    f = read_test_result_file(path, institution=institution)
    if f is None:
        return False

    print('Uploading test results for {} from {}'.format(f['config']['component'], path))

    # Check if the requested component exists, if not register
    component = None

    try:
        component = client.get("getComponent", json={'component':f['config']['component']})
    except itkdb.exceptions.ServerError:
        print('Unable to find powerboard: {}'.format(f['config']['component']))
        if not args.register:
            return False

    # Register component
    if component == None:
        nums = parse_serial(f['config']['component'])
        client.post(
            "registerComponent",
            json =
            {
                'project': 'S',
                'subproject': 'SB',
                'institution': f['config']['institution'],
                'componentType': 'PWB',
                'type': nums[3],
                'serialNumber': f['config']['component'],
            }
        )

    compinfo=client.get("getComponent", json={'component':f['config']['component']}, headers={'Cache-Control':'no-cache'})
    component=compinfo['code'] # needed for test runs list

    # Check if the config exists, if not upload
    cfgNumber=None # Configuration runNumber
    if 'runNumber' in f['config']:
        testRuns=client.get("listTestRunsByComponent", json={'component':component, 'testType':['CONFIG']})
        results=client.get('getTestRunBulk', json={'testRun':[testRun['id'] for testRun in testRuns]})


        # Final identifier for config (rn0-rn1)
        rn0=None # major, set to None if a match is found
        rn1=None # minor, set to None if a match is found
        maxrn0=0 # Known major version
        maxrn1=0 # Known minor version for found major version

        for result in results:
            if result['state']=='deleted':
                continue

            myrn0,myrn1=(int(x) for x in result['runNumber'].split('-'))

            properties={prop['code']:prop['value'] for prop in result['properties']}
            parameters={para['code']:para['value'] for para in result['results'   ]}
            gotprop =(properties==f['config']['properties'])
            gotparam=(parameters==f['config']['results'])

            maxrn0=max(maxrn0,myrn0)

            if gotprop and not gotparam:
                rn0=myrn0
                maxrn1=max(maxrn1,myrn1)

            if gotprop and gotparam:
                rn0=myrn0
                rn1=myrn1
                break

        # Determine runNumber of config
        if rn1==None:
            if rn0==None: # completely new
                rn0=maxrn0+1
                rn1=0
            else:
                rn1=maxrn1+1
            
            cfgNumber='{}-{}'.format(rn0,rn1)
            f['config']['runNumber']=cfgNumber
            f['config']['passed']=True
            print('Uploading configuration: {}'.format(cfgNumber))
            client.post("uploadTestRunResults", json=f['config'])
        else: #Found the entry!
            cfgNumber='{}-{}'.format(rn0,rn1)
            print('Using configuration: {}'.format(cfgNumber))

    # Upload the test results
    for test in f.get('tests',[]):
        # get list of test runs of current component at current stage
        currentStage = compinfo['currentStage']['code']
        r=client.get('getComponentTypeByCode', json={'project':'S', 'code':'PWB'})
        stage = next(filter(lambda stage: stage['code']==currentStage, r['stages']))
        testTypes = list(map(lambda testType: testType['testType']['code'], stage['testTypes']))
        if test['testType'] not in testTypes:
            print('Test '+test['testType'] + ' not required in stage '+currentStage+', will skip this test')

        else:
            print('Uploading test {}'.format(test['testType']))
            if test['institution'] != compinfo['currentLocation']['code']:
                print("Warning! PB is curerntly in "+compinfo['currentLocation']['code']+" but you are uploading from "+test['institution'])
            if cfgNumber is not None:
                test['properties']['CONFIG']=cfgNumber
            try:
                client.post("uploadTestRunResults", json=test)
            except Exception:
                print('Error! Failed to upload test {}'.format(test['testType']))
    print('===> UPLOADING FINISHED for {}'.format(f['config']['component'])+' <===')
    

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Upload Powerboard test data to production database.")

    parser.add_argument("test_file", help="Path to the json file with test data.")

    parser.add_argument("--register", action="store_true",
                        help="Register component if it does not exist.")

    parser.add_argument("--institution", default=None, 
                        help="Institution code to upload test results under.")
    
    args = parser.parse_args()

    c = pwbdbtools.get_db_client()

    upload_test_result_file(args.test_file, c, institution=args.institution)
