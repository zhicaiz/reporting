from database import pwbdbtools
from database import pwbdbpandas
from database import pwbdb

class Panel(pwbdb.Component):
    """ Information of panel retrieved from db """
    def __init__(self, panelid, client):
        """
        Parameters:
         panelid (str): Panel identification (VBBXXXX)
         client (itkdb.Client): Authenticated ITKDB client for performing queries
        """

        r=client.get('listComponents', json={'filterMap':{'project':'S', 'componentType':'PWB_CARRIER', 'alternativeIdentifier':panelid}})
        if r:
            panel=next(r)['code']

            self.panelType='PWB_CARRIER'
            self.pbType='PWB'
        else:
            # Retrieve panel identifier
            version, batch, number = pwbdbtools.parse_serial(panelid, fullsn=False)

            pfilt=[
                #{'code': 'VERSION','operator': '=','value': str(version)}, # does not work..
                {'code': 'BATCH'  ,'operator': '=','value': batch       },
                {'code': 'NUMBER' ,'operator': '=','value': number      }
            ]
            # r=client.get('listComponentsByProperty', json={'project':'S', 'componentType':'PB_FLEX_ARRAY', 'propertyFilter':pfilt})
            # panel=next(r)

            r=pwbdbpandas.listComponentsByProperty(client, project='S', componentType='PB_FLEX_ARRAY', propertyFilter=pfilt)
            panel=r[r.VERSION==str(version)].iloc[0].code

            self.panelType='PB_FLEX_ARRAY'
            self.pbType='PB_FLEX'

        # Initialize underlaying Component
        super(Panel, self).__init__(panel, client=client)


class Transition:
    """ Interface for performing transitions between stages """

    def __init__(self,targetStage):
        """
        Paramters:
         targetStage (str): Code of the target stage
        """
        self.targetStage=targetStage

    def check_panel(self, panel):
        """
        Perform consistency check on a panel to see whether this
        transtion is allowed.

        Parameters:
         panel (Panel): panel to check

        Returns:
         bool: True if transition is allowed, false otherwise
        """
        return self.check_panel_stage_order(panel) and self.check_tests(panel)

    def check_panel_stage_order(self, panel):
        """
        Check whether requested stage is the next for `panel` is valid.
         * must exist
         * must be current stage order + 1

        Parameters:
         panel (Panel): panel to check

        Returns:
         bool: True if transition is allowed, false otherwise
        """

        r=panel.client.get('getComponentTypeByCode', json={'project':'S', 'code':panel.panelType})

        currentStageInfo=next(filter(lambda s: s['code']==panel.currentStage, r['stages']))
        try:
            nextStageInfo=next(filter(lambda s: s['code']==self.targetStage, r['stages']))
        except StopIteration:
            print('Requested stage {} not found!'.format(self.targetStage))
            print('Available stages are:')
            for stage in r['stages']:
                star=' *' if stage['code']==currentStage else ''
                print('\t{}{}'.format(stage['code'],star))
            return False

        if currentStageInfo['order']+1!=nextStageInfo['order']:
            print('Requested stage {} is not after current stage {}.'.format(self.targetStage, currentStageInfo['code']))
            print('Available stages are:')
            for stage in filter(lambda s: s['order']==currentStageInfo['order']+1, r['stages']):
                print('\t{}'.format(stage['code']))
            return False

        return True

    def check_tests(self, panel):
        """
        Check whether all tests have results uploaded and passed.

        Parameters:
         panel (Panel): panel to check

        Returns:
         bool: True if transition is allowed, false otherwise
        """

        # Get list of required test types
        r=panel.client.get('getComponentTypeByCode', json={'project':'S', 'code':panel.pbType})

        stage = next(filter(lambda stage: stage['code']==panel.currentStage, r['stages']))
        if 'testTypes' not in stage:
            return True # There are no tests to check

        testTypes = list(map(lambda testType: testType['testType']['code'], stage['testTypes']))

        alltests=True
        for powerboard in panel.children['PWB'].values():
            testRuns=pwbdbpandas.listTestRuns(powerboard.client, 'PWB', component=powerboard.code, stage=powerboard.currentStage)
            for testType in testTypes:
                myTestRuns=testRuns[testRuns.testType==testType]

                if len(myTestRuns.index)==0:
                    print('\tMissing test result for {}. {}'.format(testType, powerboard.code))
                    alltests=False
                    continue

                myTestRuns=myTestRuns.sort_values('date', ascending=False)
                if not myTestRuns.iloc[0].passed:
                    print('\tTest {} ({}) failed. {}'.format(testType, myTestRuns.iloc[0].id, powerboard.code))
                    alltests=False
                    continue
                    

        return alltests

    def perform(self, panel):
        """
        Transition the panel and all children.

        The "all children" is a recursive statement. Any componenet associated to the
        panel in any ways is transitioned. So if a Powerboard component is associated
        (parent) to a Flex while still in the Flex Array, it is also transitioned.

        Parameters:
         panel (Panel): panel to transition
        """

        # Transition the panel object
        print('Transition panel')
        panel.currentStage=self.targetStage

        # Transition any children
        for childType,children in panel.children.items():
            for child in children.values():
                print('\tTransition child of type {}'.format(childType))
                child.currentStage=self.targetStage

                # Must transition parent Powerboards
                if panel.pbType=='PB_FLEX' and childType=='PB_FLEX': 
                    for pwb in filter(lambda p: p.componentType=='PWB', child.parents):
                        print('\t\tTransition child\'s parent of type {}'.format(pwb.componentType))
                        pwb.currentStage=self.targetStage

                # Must transition child Flex
                if panel.pbType=='PWB' and childType=='PWB':
                    for flex in child.children['PB_FLEX'].values():
                        print('\t\tTransition child\'s child of type {}'.format(flex.componentType))
                        flex.currentStage=self.targetStage

class ManLoadTransition(Transition):
    """
    SMD_LOAD -> MAN_LOAD transition

    Extra checks:
     - Must have a Powerboard parent
     - All children must have a bPOL and linPOL
    """

    def __init__(self):
        super(ManLoadTransition, self).__init__('MAN_LOAD')

    def check_panel(self, panel):
        """
        Perform consistency check on a panel to see whether this
        transtion is allowed.

        Parameters:
         panel (Panel): panel to check

        Returns:
         bool: True if transition is allowed, false otherwise
        """
        
        if not super(ManLoadTransition,self).check_panel(panel):
            return False
        return self.check_panel_smdload(panel)

    def check_panel_smdload(self, panel):
        """
        Check whether `panel` has all powerboards loaded.

        Parameters:
         panel (Panel): panel to check

        Returns:
         bool: True if transition is allowed, false otherwise
        """

        for child in panel.children['PB_FLEX'].values():
            # Find parent powerboard
            try:
                powerboard=next(filter(lambda p: p.componentType=='PWB', child.parents))
            except StopIteration:
                print('Overall powerboard object not assembled. {}'.format(child.code))
                return False

            # Check linPOL and bPOL
            if len(powerboard.children['PWB_LINPOL'])==0:
                print('Missing a PWB_LINPOL. {}'.format(powerboard.code))
                return False                

            if len(powerboard.children['BPOL12V'])==0:
                print('Missing a BPOL12V. {}'.format(powerboard.code))
                return False
            
        return True

class BondedTransition(Transition):
    """
    MAN_LOAD -> BONDED transition

    Extra checks:
     - Panel must be a Carrier Card
     - All Powerboards must have a shield box and coil
    """

    def __init__(self):
        super(BondedTransition, self).__init__('BONDED')

    def check_panel(self, panel):
        """
        Perform consistency check on a panel to see whether this
        transtion is allowed.

        Parameters:
         panel (Panel): panel to check

        Returns:
         bool: True if transition is allowed, false otherwise
        """

        if not self.check_panel_singulated(panel):
            return False        
        if not super(BondedTransition,self).check_panel(panel):
            return False
        if not self.check_panel_manload(panel):
            return False
        return True

    def check_panel_manload(self, panel):
        """
        Check whether all Powerboards contain a coil and shield box

        Parameters:
         panel (Panel): panel to check

        Returns:
         bool: True if transition is allowed, false otherwise
        """

        for powerboard in panel.children['PWB'].values():
            # Check linPOL and bPOL
            if len(powerboard.children['PWB_SHIELDBOX'])==0:
                print('Missing a PWB_SHIELDBOX. {}'.format(powerboard.code))
                return False                

            if len(powerboard.children['PWB_COIL'])==0:
                print('Missing a PWB_COIL. {}'.format(powerboard.code))
                return False                

        return True    

    def check_panel_singulated(self, panel):
        """
        Check whether panel is a Carrier Card.

        Parameters:
         panel (Panel): panel to check

        Returns:
         bool: True if transition is allowed, false otherwise
        """

        if panel.panelType!='PWB_CARRIER':
            print('Panel not singulated')
            return False
        
        return True

    def perform(self, panel):
        """
        The Flex Array component is transitioned to the SINGULATED stage.

        This is the usual transition of the panel (Carrier Card) to BONDED.

        Parameters:
         panel (Panel): panel to transition
        """

        super(BondedTransition, self).perform(panel)

        # TODO: Implement the flex array transition

class ThermalTransition(Transition):
    """
    BONDED -> THERMAL transition

    Extra checks:
     - All Powerboards must have an HVmux
     - All Powerboards must have an AMAC
     - All Powerboards electrical tests must pass
    """

    def __init__(self):
        super(ThermalTransition, self).__init__('THERMAL')

    def check_panel(self, panel):
        """
        Perform consistency check on a panel to see whether this
        transtion is allowed.

        Parameters:
         panel (Panel): panel to check

        Returns:
         bool: True if transition is allowed, false otherwise
        """

        if not self.check_panel_dieload(panel):
            return False
        if not super(ThermalTransition,self).check_panel(panel):
            return False
        return True

    def check_panel_dieload(self, panel):
        """
        Check whether all Powerboards contain an HVmux and AMAC

        Parameters:
         panel (Panel): panel to check

        Returns:
         bool: True if transition is allowed, false otherwise
        """

        for powerboard in panel.children['PWB'].values():
            if len(powerboard.children['HVMUX'])==0:
                print('Missing an HVMUX. {}'.format(powerboard.code))
                return False                

            if len(powerboard.children['AMAC'])==0:
                print('Missing an AMAC. {}'.format(powerboard.code))
                return False                

        return True

    
available={}
available['RCP'       ]=Transition('RCP')
available['SMD_LOAD'  ]=Transition('SMD_LOAD')
available['MAN_LOAD'  ]=ManLoadTransition()
available['BONDED'    ]=BondedTransition()
available['THERMAL'   ]=ThermalTransition()
available['BURN_IN'   ]=Transition('BURN_IN')
available['MODULE_RCP']=Transition('MODULE_RCP')

