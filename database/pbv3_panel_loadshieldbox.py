#!/usr/bin/env python

import os, sys
import argparse
import itkdb
import random

from database import pwbdb
from database import pwbdbtools
from database import pwbdbpandas
from database import transitions

def loadshieldbox(client, panelid, position, pb_num):
    """
    Manually load a powerboard with a marked shield box.
    
    Parameters:
     client (itkdb.Client): Authenticated ITk DB client
     panelid (str): Panel ID to load (VBBXXXX)
     position (int): Powerboard position
     pb_num (int) Powerboard number at position
    """

    #
    # Find panel component
    panel = transitions.Panel(panelid, client=client) 

    # Find the necessary shield box
    version=panel.property('VERSION')
    batch  =panel.property('BATCH')

    pfilt=[
        #{'code': 'VERSION','operator': '=','value': str(version)}, # does not work..
        {'code': 'BATCH'     ,'operator': '=','value': batch       },
        {'code': 'PB_NUMBWE' ,'operator': '=','value': pb_num      }
        ]

    # r=client.get('listComponentsByProperty', json={'project':'S', 'componentType':'PB_FLEX_ARRAY', 'propertyFilter':pfilt})
    # panel=next(r)

    r=pwbdbpandas.listComponentsByProperty(client, project='S', componentType='PWB_SHIELDBOX', propertyFilter=pfilt)
    if len(r.index)==0 or len(r[r.VERSION==str(version)].index)==0:
        raise Exception('Unable to find a shiled box with the corresponding markings.')

    sbs=r[r.VERSION==str(version)]
    sb=pwbdb.Component(sbs.iloc[0].code, client=client)
    if len(sb.parents)>0:
        raise Exception('Found shield box ({}) already has a parent!'.format(sb.code))

    print('Found a shield box: {}'.format(sb.code))    

    # Assemble :)
    flex=panel.children['PB_FLEX'][position]
    powerboard=next(filter(lambda p: p.componentType=='PWB', flex.parents))
    if len(powerboard.children['PWB_SHIELDBOX'])>0:
        raise Exception('Powerboard ({}) already has a shield box!'.format(powerboard.code))

    print('Assembling to Powerboard {}'.format(powerboard.code))
    powerboard.assemble(sb)
    powerboard.property('PB_NUMBWE', pb_num)

    client.post('updateComponentSN',json={'component':powerboard.code, 'serialNumber':pwbdbtools.create_serial(version, batch, pb_num)})

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Manually load a Powerboard.")

    parser.add_argument("panelid"           , help="Panel identifier (VBBXXXX).")
    parser.add_argument("position", type=int, help="Powerboard position.")
    parser.add_argument("pb_num"  , type=int, help="Powerboard number at given position.")

    args = parser.parse_args()

    c = pwbdbtools.get_db_client()

    loadshieldbox(c, args.panelid, args.position, args.pb_num)
