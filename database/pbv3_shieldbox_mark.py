#!/usr/bin/env python

import os, sys
import argparse
import itkdb

from database import pwbdbtools
from database import pwbdbpandas
from database import pwbdb

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Mark shield boxes with ID.")

    parser.add_argument("type"              , help="Shield Box type code.")
    parser.add_argument("version" , type=int, help="PWB version number.")
    parser.add_argument("batch"   , type=int, help="PWB batch number.")
    parser.add_argument("pwb_num"           , help="Comma-separated PWB numbers. Ranges are allowed (ie: 1,3-5).")

    args = parser.parse_args()

    c = pwbdbtools.get_db_client()

    pb_nums=set(pwbdbtools.parse_pwb_list(args.pwb_num))


    # 
    # Get existing shieldboxes and check for overlap
    pfilt=[
        #{'code': 'VERSION','operator': '=','value': str(version)}, # does not work..
        {'code': 'BATCH'     ,'operator': '=','value': args.batch}
        ]

    existing_shieldboxes=pwbdbpandas.listComponentsByProperty(c, 'P', componentType='PWB_SHIELDBOX', propertyFilter=pfilt)
    existing_shieldboxes['VERSION'  ]=existing_shieldboxes.VERSION  .astype(int)
    existing_shieldboxes['PB_NUMBWE']=existing_shieldboxes.PB_NUMBWE.astype(int)

    existing_shieldboxes=existing_shieldboxes[existing_shieldboxes.VERSION==args.version]
    existing_shieldboxes=existing_shieldboxes[existing_shieldboxes.type==args.type]
    existing_shieldboxes=set(existing_shieldboxes.PB_NUMBWE.values)
    pb_nums_existing=pb_nums&existing_shieldboxes
    pb_nums         =pb_nums-existing_shieldboxes

    #
    # Print existing
    for pb_num in pb_nums_existing:
        print("Skip existing {:01d}{:02d} {:04d}".format(args.version, args.batch, pb_num))
    
    #
    # Start marking
    shieldbox_codes=pwbdbtools.get_random_components(c, 'SB', 'PWB_SHIELDBOX', args.type, len(pb_nums), filt={'currentStage':'MANUFACTURED'})
    shieldboxes=map(lambda sb_code: pwbdb.Component(sb_code, client=c), shieldbox_codes)

    for pb_num in pb_nums:
        print("Marking {:01d}{:02d} {:04d}".format(args.version, args.batch, pb_num))
        sb=next(shieldboxes)
        sb.property('VERSION'  , str(args.version))
        sb.property('BATCH'    , args.batch)
        sb.property('PB_NUMBWE', pb_num)
        sb.currentStage='MARKED'
