# v1.9.1
- Fix equality comparison when checking consistance with test institute and location
# v1.9.0
- Add a upload test data button
- Add a button to set institution and pass it to `pbv3_mass_test`
# v1.8.1
- Fix --hbipc option support in PowertoolsRunner command building
# v1.8.0
- Show error messages for Auto Fill button in GUI
- Add hybrid burn-in powerboard carrier (hbipc) switch to enable hbipc test sequence in the GUI
# v1.7.0
- Add UI for powersupply initialization and I2C commuication checks
