#!/usr/bin/env python

import matplotlib.pyplot as plt
import numpy as np
import argparse
import sys, os

from pbv3 import tests

parser=argparse.ArgumentParser(description="Plot PBv3 status as a function of time from many monitoring files.")
parser.add_argument("input", nargs="+",
                        help="File with monitoring results.")

args=parser.parse_args()

data=tests.load_testresults([datapath for datapath in args.input if os.stat(datapath).st_size > 0], testType='STATUS')
tests.apply_calibrations(data)
tests.apply_conversions(data)

#
# Plot PTAT calibration
subdata=data[(data['DCDCen']==0)&(data['AMACNTCPB']<1023)&(data['AMACPTAT']>100)]
ptatcal=np.poly1d(np.polyfit(subdata['AMACPTATvolts'],subdata['AMACNTCPBtemp'],1))

data['AMACPTATtemp']=ptatcal(data['AMACPTATvolts'])

plt.plot(subdata['AMACNTCPBtemp'],subdata['AMACPTATvolts'],'.')
plt.xlabel('NTCpb Temperature [°C]')
plt.ylabel('PTAT [mV]')
plt.savefig('calptat.pdf')
plt.show()
plt.clf()

#
# Plot CTAT calibration
subdata=data[(data['DCDCen']==0)&(data['AMACNTCPB']<1023)&(data['AMACCTAT']<700)]
ctatcal=np.poly1d(np.polyfit(subdata['AMACCTATvolts'],subdata['AMACNTCPBtemp'],1))

data['AMACCTATtemp']=ctatcal(data['AMACCTATvolts'])

plt.plot(subdata['AMACNTCPBtemp'],subdata['AMACCTATvolts'],'.')
plt.xlabel('NTCpb Temperature [°C]')
plt.ylabel('CTAT [mV]')
plt.savefig('calctat.pdf')
plt.show()
plt.clf()

# actually plot it
plt.plot(data['TIMESTART'],data['AMACNTCPBtemp'],'.',label='NTCpb')
plt.plot(data['TIMESTART'],data['AMACPTATtemp' ],'.',label='PTAT')
plt.plot(data['TIMESTART'],data['AMACCTATtemp' ],'.',label='CTAT')

plt.xlabel('Time')
plt.ylabel('Temperature [°C]')
plt.ylim(-50,50)
plt.grid(True,axis='y')
plt.legend(frameon=False)

plt.savefig('temperature.pdf')
plt.show()
plt.clf()

#
# Plot
plt.plot(data['TIMESTART'],data['AMACAM600BGvolts'],'.')
plt.xlabel('Time')
plt.ylabel('AMBG')
plt.savefig('am600bg.pdf')
plt.show()
plt.clf()

#
# Plot
plt.plot(data['TIMESTART'],data['AMACVDDLRvolts'],'.')
plt.xlabel('Time')
plt.ylabel('linPOL Output [mV]')
plt.savefig('vddlr.pdf')
plt.show()
plt.clf()

#
# Plot
plt.plot(data['TIMESTART'],data['AMACVDCDCvolts'],'.')
plt.xlabel('Time')
plt.ylabel('DC/DC Output [mV]')
plt.savefig('vdcdc.pdf')
plt.show()
plt.clf()

#
# Plot
plt.plot(data['TIMESTART'],data['AAMCDCDCINvolts'],'.')
plt.xlabel('Time')
plt.ylabel('Vin [mV]')
plt.savefig('dcdcin.pdf')
plt.show()
plt.clf()

#
# Plot
plt.plot(data['TIMESTART'],data['AMSLOPE'],'.')
plt.xlabel('Time')
plt.ylabel('AM Slope [mV/count]')
plt.savefig('amslope.pdf')
plt.show()
plt.clf()

#
# Plot
plt.plot(data['TIMESTART'],data['AMACHVRETvolts'],'.')
plt.xlabel('Time')
plt.ylabel('HV Sensor Current [counts]')
plt.savefig('hvret.pdf')
plt.show()
plt.clf()

#
# Plot
for i in range(16):
    plt.plot(data['TIMESTART'],data['AMOFFSET{}'.format(i)],'.',label=i)
plt.xlabel('Time')
plt.ylabel('AM offset [count]')
plt.legend(frameon=False)
plt.savefig('amoffset.pdf')
plt.show()
plt.clf()

