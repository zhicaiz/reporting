import json
import numpy as np

import matplotlib.pyplot as plt
import pandas as pd
import json
import glob
import sys, os

def load_testresults(path, testType):
    """
    Load powerboard test results for a specific test from a file(s).

    The input file can either be a JSON file with results from powertools or a 
    CSV file that is a result of a previous call to `load_testresults`. It can
    also be a list of paths to a combination of both types.

    In the case of JSON files, the `config` and `tests` blocks are reformatted and
    merged into a columnar format.

    The following extra columns are added:
     - `config` block is merged with the results
     - `runName` corresponding to the result file start time 
    
    In the case of a CSV file, it is loaded directly using `pd.read_csv`.

    The result is a Pandas DataFrame containing the columns (both properties and results)
    corresponding to all instances of the testType in the result file.

    Parameters:
    path (string or list): path(s) to the JSON file
    testType (string): name of test type to extract

    Returns:
    DataFrame: all test results correspoing to testType
    """

    if type(path)!=list:
        path=[path]

    cdatas    =[]
    json_datas=[]
    csv_datas =[]
    for datapath in path:
        if datapath.endswith('.json'):
            jdata=json.load(open(datapath))

            if 'config' in jdata:
                # Parse configuration data
                cdata=jdata['config']['results']

                # Flatten certain structures
                cdata.update({('AMOFFSET{}'.format(i),cdata['AMOFFSET'][i]) for i in range(len(cdata['AMOFFSET']))})
                cdata.pop('AMOFFSET')
                if 'CTAT0' in cdata:
                    cdata.update({('CTAT0{}'.format(i),cdata['CTAT0'][i]) for i in range(len(cdata['CTAT0']))})
                    cdata.pop('CTAT0')

                cdata.update(jdata['config']['properties'])
                cdata=pd.DataFrame(data=cdata,index=[jdata['time']['start']])
                cdata['component']=jdata['config'].get('component','')
                cdatas.append(cdata)

            if 'tests' in jdata:
                # Add test results
                data=pd.json_normalize(jdata,record_path='tests')
                data=data[data.testType==testType].dropna(axis=1).apply(pd.Series.explode).convert_dtypes().reset_index()
                data.columns=[colname.replace('results.','').replace('properties.','') for colname in data.columns]
                data['runName']=jdata['time']['start']

                json_datas.append(data)
        else:
            data=pd.read_csv(datapath,converters={'TIMESTART':pd.to_datetime})
            csv_datas.append(data)

    # Extra post-processing for powertools results
    if len(json_datas)>0:
        # Merge everything
        data =pd.concat(json_datas, ignore_index=True)
        cdata=pd.concat(cdatas)

        # helpful conversions
        data['TIMESTART']=pd.to_datetime(data['TIMESTART'],format='%Y_%m_%d-%H:%M:%SZ%z')

        # Merge with config
        data=data.merge(cdata,left_on='runName',right_index=True,suffixes=('','_config'))

        csv_datas.append(data)

    return pd.concat(csv_datas,ignore_index=True)

#
# Calibration functions
channel_map={}
channel_map['AMACVDCDC'    ]= 0
channel_map['AMACVDDLR'    ]= 1
channel_map['AMACVDDREG'   ]= 3
channel_map['AAMCDCDCIN'   ]= 2
channel_map['AMACAM600BG'  ]= 4
channel_map['AMACCTAT'     ]= 6
channel_map['AMACNTCX'     ]= 7
channel_map['AMACNTCY'     ]= 8
channel_map['AMACNTCPB'    ]= 9
channel_map['AMACCUR10V'   ]=12
channel_map['AMACCUR10VTPH']=12
channel_map['AMACCUR10VTPL']=12
channel_map['AMACCUR1V'    ]=13
channel_map['AMACCUR1VTPH' ]=13
channel_map['AMACCUR1VTPL' ]=13
channel_map['AMACHVRET'    ]=14
channel_map['AMACPTAT'     ]=15

def apply_calibration(data, colname, ch):
    """
    Apply counts -> mV calibration to an AMAC measurement.

    The input dataframe is modified in place with new columns
    being AMACMEASUREMENTvolts, where AMACMEASUREMENT corresponds
    to the original column name.

    Parameters:
    data (DataFrame): Dataframe with results and calibrations
    colname (str): Name of the column to calibrate
    ch (Series): AM channel of the column
    """
    data['{}volts'.format(colname)]=(data[colname]-data['AMOFFSET{}'.format(ch)])*data['AMSLOPE']    

def apply_conversions(data):
    """
    Convert AMAC measurements into sensible values.

    Parameters:
    data (DataFrame): dataframe with results to convert
    """

    if 'AMACVDDLRvolts' in data.columns:
        data['AMACVDDLRvolts']=data['AMACVDDLRvolts']*2

    if 'AMACVDCDCvolts' in data.columns:
        data['AMACVDCDCvolts']=data['AMACVDCDCvolts']*2

    if 'AAMCDCDCINvolts' in data.columns:
        data['AAMCDCDCINvolts']=data['AAMCDCDCINvolts']*15

    if 'AMACVDDREGvolts' in data.columns:
        data['AMACVDDREGvolts']=data['AMACVDDREGvolts']*3/2

    if 'AMACCUR10V' in data.columns:
        apply_conv_cur10v(data)

    if 'AMACCUR1V' in data.columns:
        apply_conv_cur1v(data)
        
    if 'AMACHVRET' in data.columns:
        apply_conv_hvret(data)
        
    if 'AMACNTCPB' in data.columns:
        apply_conv_ntc(data, 'NTCPB', 3900)

    if 'AMACNTCX' in data.columns:
        apply_conv_ntc(data, 'NTCX', 3350)

    if 'AMACNTCY' in data.columns:
        apply_conv_ntc(data, 'NTCY', 3350)

    if 'AMACPTAT' in data.columns:
        apply_conv_ptat(data,  8.5)

    if 'AMACCTAT' in data.columns:
        apply_conv_ctat(data, -1.5)

def apply_calibrations(data):
    """
    Apply counts -> mV calibration to AMAC measurements.

    The standardized column names from `channel_map` are used.

    The input dataframe is modified in place with new columns
    being AMACMEASUREMENTvolts, where AMACMEASUREMENT corresponds
    to the original column defined above.

    Parameters:
    data (DataFrame): dataframe with results to convert
    """

    global channel_map
    for chname,ch in channel_map.items():
        if chname not in data: continue
        apply_calibration(data, chname, ch)

def apply_conv_cur10v(data):
    """
    Convert AMAC measurement of input current from mV to A.

    See `conv_cur10v` functions for details on the calculation.

    This function assumes:
    - `gain=10.4`
    - `R=33.4 mΩ`.

    Assumes the presence of the following columns:
     - `AMACCUR10Vvolts`: AM reading in mV
     - `CUR10VOFFSET`: AM reading at "zero" current

    The following column will be created in `data`:
     - AMACCUR10Vcurr`: Current in A

    Parameters:
    data (DataFrame): dataframe with results to convert
    """
    data['AMACCUR10Vcurr']=conv_curv(
        data['AMACCUR10Vvolts'],
        data['CUR10VOFFSET'],
        10.4,
        33.4
        )

def apply_conv_cur1v(data):
    """
    Convert AMAC measurement of output current from mV to A.

    See `conv_cur1v` functions for details on the calculation.

    This function assumes:
    - `gain=30`
    - `R=7.6 mΩ`.

    Assumes the presence of the following columns:
     - `AMACCUR1Vvolts`: AM reading in mV
     - `CUR1VOFFSET`: AM reading at "zero" current

    The following column will be created in `data`:
     - AMACCUR1Vcurr`: Current in A

    Parameters:
    data (DataFrame): dataframe with results to convert
    """
    data['AMACCUR1Vcurr']=conv_curv(
        data['AMACCUR1Vvolts'],
        data['CUR1VOFFSET'],
        30,
        7.6
        )
    
def apply_conv_hvret(data):
    """
    Convert AMAC measurement of HV current from mV to A

    Assumes the presence of the following columns:
     - `AMACHVRETvolts`: AM reading in mV
     - `HVcurGain`: Gain set in the AMAC.

    The following column will be created in `data`:
     - AMACHVRETcurr`: Current in A

    Parameters:
    data (DataFrame): dataframe with results to convert
    """
    data['AMACHVRETcurr']=conv_hvret(
        data['AMACHVRETvolts'],
        data['HVcurGain'].values
        )

def apply_conv_ntc(data, ntc, B):
    """
    Convert AMAC measurement of temperature from mV to C

    The `ntc` argument (`NTC` below) specifies which columns to get informatino
    from. The important names are:
     - `AMACNTCvolts`: AM reading in mV
     - `NTCSenseRange`: Sense range set in the AMAC. Here `NTC[3:]` are converted to lower case.
     - `NTC`: The calibrated zero value of the NTC block  in mV

    The following column will be created in `data`:
     - `AMACNTCtemp`: Temperature in C

    Parameters:
    data (DataFrame): dataframe with results to convert
    ntc (str): name of the NTC to convert (ie: NTCpb)
    B (float): B parameter of the conntected NTC
"""
    data['AMAC{}temp'.format(ntc)]=conv_temperature(
        data['AMAC{}volts'.format(ntc)],
        data['{}'.format(ntc)],
        data['{}{}SenseRange'.format(ntc[0:3],ntc[3:].lower().replace('x','x0').replace('y','y0'))].values,
        B
        )

def apply_conv_ptat(data, ptatslope=8.5):
    """
    Convert PTAT measurement of temperature from mV to C

    Assumes the presence of the following columns
     - `AMACPTATvolts`: AM reading in mV
     - `PTAT0`: PTAT mV at 0C

    The following column will be created in `data`:
     - `AMACPTATtemp`: Temperature in C

    Parameters:
    data (DataFrame): dataframe with results to convert
    ptatslope (float): PTAT slope (mV/C) to assume
"""
    data['AMACPTATtemp']=(data['AMACPTATvolts']-data['PTAT0'])/ptatslope

def apply_conv_ctat(data, ctatslope=-1.5):
    """
    Convert CTAT measurement of temperature from mV to C

    Assumes the presence of the following columns
     - `AMACCTATvolts`: AM reading in mV
     - `CTAT0offset`: CTAT mV at 0C, where offset is the offset value in decimal
     - `CTAToffset`: register setting for the CTAT offset

    The following column will be created in `data`:
     - `CPTAT0`: CTAT0 for the selected offset
     - `AMACPTATtemp`: Temperature in C

    Parameters:
    data (DataFrame): dataframe with results to convert
    ctatslope (float): CTAT slope (mV/C) to assume
"""
    data['CTAT0']=data.apply(lambda row: row['CTAT0{}'.format(row['CTAToffset'])], axis=1)

    data['AMACCTATtemp']=(data['AMACCTATvolts']-data['CTAT0'])/ctatslope

def conv_curv(AMACCURVvolts, CURVOFFSET, gain=10.4, Rsense=33.4):
    """
    Convert AMAC measurement of input current from mV to A.

    The conversion is implemented using the following equation:
    ```
    (AMACCURVvolts-CURVOFFSET)/(gain*Rsense)
    ```

    Parameters:
     - AMACCURVvolts (float): AM reading in mV
     - CURVOFFSET (float): AM reading at "zero" current in mV
     - gain (float): Gain of amplifier
     - Rsense (float): Resistance of input sense resistor

    Return:
    float: Input current in A
    """
    return (AMACCURVvolts-CURVOFFSET)/(gain*Rsense)

def hvret_Rfb(HVgain):
    """
    Calculate the feedback resistor value for a given HVgain register setting.

    All arguments can be arrays/Series to convert multiple measurements. In
    this case, values are matched by index.

    Parameters:
    HVgain (int): HV sense gain set in the AMAC. 

    Return:
    float: Feedback resistance in Ohms
    """  
    # Get bits of enabled gain resistors
    S0=(HVgain>>0)&1
    S1=(HVgain>>1)&1
    S2=(HVgain>>2)&1
    S3=(HVgain>>3)&1

    # gain resistor value
    R=200e3
    R3=120
    R2=300
    R1=1.8e3
    R0=19e3

    Rgain=1/(1/R+S0/R0+S1/R1+S2/R2+S3/R3)

    return Rgain

def conv_hvret(HVretvolt, HVgain):
    """
    Convert AMAC measurement of HV current from mV to A

    All arguments can be arrays/Series to convert multiple measurements. In
    this case, values are matched by index.

    Parameters:
    HVretvolt (float): AMAC reading of HVret in mV
    HVgain (int): HV sense gain set in the AMAC. 

    Return:
    float: Current in A
    """

    Rgain=hvret_Rfb(HVgain)

    # Calculate
    current=HVretvolt*1e-3/Rgain

    return current
    
def conv_temperature(NTCvolt, NTCcal, SenseRange, B):
    """
    Convert AMAC measurement of temperature from mV to C

    All arguments can be arrays/Series to convert multiple measurements. In
    this case, values are matched by index.

    Parameters:
    NTCvolt (float): AMAC reading of NTC channel in mV
    NTCcal: (float): The calibrated zero value of the NTC block  in mV
    NTCSenseRange (int): Sense range set in the AMAC. 
    B (float): B parameter of the conntected NTC

    Return:
    float: Temperature in C
"""
    # Get bits of enabled gain resistors
    NTCS0=(SenseRange>>0)&1
    NTCS1=(SenseRange>>1)&1
    NTCS2=(SenseRange>>2)&1

    # AMAC NTC block gain resistors
    R=200e3
    R0=133e3
    R1=50e3
    R2=22.22e3

    Rgain=1/(1/R+NTCS0/R0+NTCS1/R1+NTCS2/R2)

    # NTC resistance
    Rntc=Rgain*NTCcal/(NTCvolt-NTCcal)-280-250

    # And finally calculate temperature
    T0=275.15
    R25=10e3
    T25=T0+25
    temp=1./(np.log(Rntc/R25)/B+1/T25)-T0

    return temp
