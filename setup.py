#!/usr/bin/env python

import setuptools

setuptools.setup(
    name='PBv3',
    version='1.9.1',
    description='Python code for making and analysing ITk Strips PBv3 test results.',
    install_requires=[
        'matplotlib',
        'pandas',
        'pyyaml',
        'dash',
        'dash_auth',
        'dash_uploader',
        'itkdb>=0.3.6',
        'httptime',
        'pillow'
      ],
    packages=[
        'pbv3',
        'database',
        'webreport/pwbDashCommon',
        'webreport/pwbDashGUI',
        'webreport/inventory'
      ],
    package_data={
        'webreport/pwbDashGUI' : ['assets/clientside.js', 'assets/style.css', 'template/*.html'],
        'webreport/inventory'  : ['template/*.html', 'template/static']
        },
    scripts=['tools/pbv3_plot_monitor.py', 'tools/pbv3_convert.py',
             'database/pbv3_massregister.py','database/pbv3_shieldbox_mark.py',
             'database/pbv3_panel_loadamac.py', 'database/pbv3_panel_loadcoils.py','database/pbv3_panel_loadhvmux.py','database/pbv3_panel_loadshieldbox.py','database/pbv3_panel_singulate.py','database/pbv3_panel_smdload.py','database/pbv3_panel_transition.py', 'database/pbv3_upload_picture.py',
             'database/pbv3_upload_tests.py',
             'database/pbv3_create_testtype.py',
             'webapps/PWBTestingGUI.py', 'webreport/inventory/inventory.py']
)
