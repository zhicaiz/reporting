#!/usr/bin/env python3

import os, sys
import argparse
from webreport import pwbDashGUI

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Powerboard Test System Configuration GUI")
    parser.add_argument('-c','--config'         , default='config.json', help='Configuration file.')
    parser.add_argument('-p','--port'  ,type=int, default=5000         , help='Port for web server.')
    parser.add_argument('-d','--debug' ,action='store_true'            , help='Run in debug mode.')

    args = parser.parse_args()

    app = pwbDashGUI.create_app(args.config)
    app.server.run(host="0.0.0.0", port=args.port, debug=args.debug)
