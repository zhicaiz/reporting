#!/usr/bin/env python3

if __name__ == '__main__':
    from __path__ import updatePath
    updatePath()

import itk_pdb.dbAccess as dbAccess
from itk_pdb.dbAccess import ITkPDSession

import time
import itkdb
import os
import sys
import getpass
import json

def get_components(comp_type, sub_type, itemCount, lib):
  try:
    unassembled = []
    listTime = []
    getTime = []
    i = 0
    page = 0
    pageSize = 200
    # Append parentless components to unassembled until itemCount is reached
    while i == 0:
      if lib=='p':
        now = time.time()
        comp_list = session.doSomething(action = "listComponents", data = {'project':'S', 'subproject':'SG', 'componentType':comp_type, 'type':sub_type, 'assembled': False, 'pageInfo':{'pageSize':pageSize, 'pageIndex':page}}, method='GET')
        listTime.append(time.time()-now)
      if lib=='c':
        now = time.time()
        comp_list = c.get("listComponents", json = {'project':'S', 'subproject':'SG', 'componentType':comp_type, 'type':sub_type, 'assembled': False, 'pageInfo':{'pageSize':pageSize, 'pageIndex':page}})
        listTime.append(time.time()-now)
      if lib=='i':
        now = time.time()
        comp_list = c.get("listComponents", json = {'project':'S', 'subproject':'SG', 'componentType':comp_type, 'type':sub_type, 'assembled': False, 'pageInfo':{'pageSize':pageSize, 'pageIndex':page}}, headers={'Cache-Control':'no-cache'})
        listTime.append(time.time()-now)

      n = 0
      #production_databse_scripts
      if lib=='p':
        for component in comp_list:
          n=n+1
          now = time.time()
          get_component = session.doSomething(action = "getComponent", data = {'component':component['code']}, method='GET')
          getTime.append(time.time()-now)

          if get_component['trashed'] == False:
            unassembled.append(get_component)
          if len(unassembled) == itemCount:                  
            i = 1
            break

      #itkdb with caching
      if lib=='c':
        for component in comp_list:
          n=n+1
          now = time.time()
          get_component = c.get("getComponent", json = {'component':component['code']})
          getTime.append(time.time()-now)
        
          if get_component['trashed'] == False:
            unassembled.append(get_component)
          if len(unassembled) == itemCount:                  
            i = 1
            break

      #itkdb without caching
      if lib=='i':
        for component in comp_list:
          n=n+1
          now = time.time()
          get_component = c.get("getComponent", json = {'component':component['code']}, headers={'Cache-Control':'no-cache'})
          getTime.append(time.time()-now)

          if get_component['trashed'] == False:
            unassembled.append(get_component)
          if len(unassembled) == itemCount:                  
            i = 1
            break

      # If listComponents does not return a list of length pageSize, we are on the last page
      # so do not call listComponents again.
      if n<pageSize:
        i = 1

      page = page+1

    return [listTime, getTime]

  except Exception as e:
    print('Problem finding '+ comp_type + ', ' + sub_type +' in database. Received the following error: ')
    print(e)
    sys.exit()

if __name__ == "__main__":
  if os.environ.get('ITK_DB_AUTH'):
      dbAccess.token = os.getenv('ITK_DB_AUTH')
        
  global session
  session = ITkPDSession()
  session.authenticate()

  if os.environ.get('ITKDB_ACCESS_CODE1') and os.environ.get('ITKDB_ACCESS_CODE2'):
      accessCode1 = os.environ.get('ITKDB_ACCESS_CODE1')
      accessCode2 = os.environ.get('ITKDB_ACCESS_CODE2')
  else:
      accessCode1 = getpass.getpass("AccessCode1: ")
      accessCode2 = getpass.getpass("AccessCode2: ")
  
  u = itkdb.core.User(accessCode1 = accessCode1, accessCode2 = accessCode2)
  c = itkdb.Client(user = u, expires_after=dict(days=1))
  c.user.authenticate()

  itemCount = 50
  iterationCount = 10

  getComp = []
  getCompITKDB = []
  getCompITKDBCa = []

  #production_database_scripts
  bpoll = []
  bpolg = []
  hvl = []
  hvg = []
  lpl = []
  lpg = []
  cl = []
  cg = []
  sl = []
  sg = []
  totl = []
  totg = []
  for i in range(0,iterationCount):
    now = time.time()

    bpol = get_components('BPOL12V','FEAST11', itemCount, 'p')
    hvmux = get_components('HVMUX','HVMUX1', itemCount, 'p')
    linpol = get_components('PWB_LINPOL','LINPOL12V', itemCount, 'p')
    coil = get_components('PWB_COIL','COILV3A', itemCount, 'p')
    shield = get_components('PWB_SHIELDBOX','BARRELSHIELDBOX3A', itemCount, 'p')

    getComp.append(time.time()-now)

    bpoll = bpoll + bpol[0]
    bpolg = bpolg + bpol[1]
    hvl = hvl + hvmux[0]
    hvg = hvg + hvmux[1]
    lpl = lpl + linpol[0]
    lpg = lpg + linpol[1]
    cl = cl + coil[0]
    cg = cg + coil[1]
    sl = sl + shield[0]
    sg = sg + shield[1]

  totl = bpoll+hvl+lpl+cl+sl
  totg = bpolg+hvg+lpg+cg+sg

  pds = [getComp, totl, totg, bpoll, bpolg, hvl, hvg, lpl, lpg, cl, cg, sl, sg]

  #itkdb without caching
  bpollI = []
  bpolgI = []
  hvlI = []
  hvgI = []
  lplI = []
  lpgI = []
  clI = []
  cgI = []
  slI = []
  sgI = []
  totlI = []
  totgI = []
  for i in range(0,iterationCount):
    now = time.time()

    bpol = get_components('BPOL12V', 'FEAST11', itemCount, 'i')
    hvmux = get_components('HVMUX', 'HVMUX1', itemCount, 'i')
    linpol = get_components('PWB_LINPOL', 'LINPOL12V', itemCount, 'i')
    coil  = get_components('PWB_COIL', 'COILV3A', itemCount, 'i')
    shield = get_components('PWB_SHIELDBOX', 'BARRELSHIELDBOX3A', itemCount, 'i')

    getCompITKDB.append(time.time()-now)

    bpollI = bpollI + bpol[0]
    bpolgI = bpolgI + bpol[1]
    hvlI = hvlI + hvmux[0]
    hvgI = hvgI + hvmux[1]
    lplI = lplI + linpol[0]
    lpgI = lpgI + linpol[1]
    clI = clI + coil[0]
    cgI = cgI + coil[1]
    slI = slI + shield[0]
    sgI = sgI + shield[1]

  totlI = bpollI+hvlI+lplI+clI+slI
  totgI = bpolgI+hvgI+lpgI+cgI+sgI

  ITKDB = [getCompITKDB, totlI, totgI, bpollI, bpolgI, hvlI, hvgI, lplI, lpgI, clI, cgI, slI, sgI]

  #itkdb with caching
  bpollICa = []
  bpolgICa = []
  hvlICa = []
  hvgICa = []
  lplICa = []
  lpgICa = []
  clICa = []
  cgICa = []
  slICa = []
  sgICa = []
  totlICa = []
  totgICa = []
  for i in range(0,iterationCount):
    now = time.time()

    bpol = get_components('BPOL12V', 'FEAST11', itemCount, 'c')
    hvmux = get_components('HVMUX', 'HVMUX1', itemCount, 'c')
    linpol = get_components('PWB_LINPOL', 'LINPOL12V', itemCount, 'c')
    coil = get_components('PWB_COIL', 'COILV3A', itemCount, 'c')
    shield = get_components('PWB_SHIELDBOX', 'BARRELSHIELDBOX3A', itemCount, 'c')

    getCompITKDBCa.append(time.time()-now)

    bpollICa = bpollICa + bpol[0]
    bpolgICa = bpolgICa + bpol[1]
    hvlICa = hvlICa + hvmux[0]
    hvgICa = hvgICa + hvmux[1]
    lplICa = lplICa + linpol[0]
    lpgICa = lpgICa + linpol[1]
    clICa = clICa + coil[0]
    cgICa = cgICa + coil[1]
    slICa = slICa + shield[0]
    sgICa = sgICa + shield[1]

  totlICa = bpollICa+hvlICa+lplICa+clICa+slICa
  totgICa = bpolgICa+hvgICa+lpgICa+cgICa+sgICa

  ITKDBCa = [getCompITKDBCa, totlICa, totgICa, bpollICa, bpolgICa, hvlICa, hvgICa, lplICa, lpgICa, clICa, cgICa, slICa, sgICa]

  data = {}
  data['pds'] = pds
  data['ITKDB'] = ITKDB
  data['ITKDBCa'] = ITKDBCa

  with open('data/assembleComponents.json', 'w+') as f:
    json.dump(data, f)
