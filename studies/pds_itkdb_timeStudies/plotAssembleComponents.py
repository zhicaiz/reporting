#!/usr/bin/env python3

import os
import json
import numpy as np
import matplotlib.pyplot as plt
from csv import reader

with open('data/assembleComponents.json', 'r') as f:
  data = json.load(f)

  getComp = data['pds'][0]
  getCompI = data['ITKDB'][0]
  getCompICa = data['ITKDBCa'][0]

  totl = data['pds'][1]
  totlI = data['ITKDB'][1]
  totlICa = data['ITKDBCa'][1]

  totg = data['pds'][2]
  totgI = data['ITKDB'][2]
  totgICa = data['ITKDBCa'][2]

  bpoll = data['pds'][3]
  bpollI = data['ITKDB'][3]
  bpollICa = data['ITKDBCa'][3]

  bpolg = data['pds'][4]
  bpolgI = data['ITKDB'][4]
  bpolgICa = data['ITKDBCa'][4]

  hvl = data['pds'][5]
  hvlI = data['ITKDB'][5]
  hvlICa = data['ITKDBCa'][5]

  hvg = data['pds'][6]
  hvgI = data['ITKDB'][6]
  hvgICa = data['ITKDBCa'][6]

  lpl = data['pds'][7]
  lplI = data['ITKDB'][7]
  lplICa = data['ITKDBCa'][7]

  lpg = data['pds'][8]
  lpgI = data['ITKDB'][8]
  lpgICa = data['ITKDBCa'][8]

  cl = data['pds'][9]
  clI = data['ITKDB'][9]
  clICa = data['ITKDBCa'][9]

  cg = data['pds'][10]
  cgI = data['ITKDB'][10]
  cgICa = data['ITKDBCa'][10]

  sl = data['pds'][11]
  slI = data['ITKDB'][11]
  slICa = data['ITKDBCa'][11]

  sg = data['pds'][12]
  sgI = data['ITKDB'][12]
  sgICa = data['ITKDBCa'][12]

  bins = np.histogram(np.hstack((getComp, getCompI, getCompICa)), bins = 'auto')[1]
  plt.hist(x=getComp, bins=bins, alpha=0.5, label = 'production_database_scripts: Mean ' + str(round(sum(getComp)/len(getComp),2)))
  plt.hist(x=getCompI, bins=bins, alpha=0.5, label = 'itkdb: Mean ' + str(round(sum(getCompI)/len(getCompI),2)))
  plt.hist(x=getCompICa, bins=bins, alpha=0.5, label = 'itkdb Cached: Mean ' + str(round(sum(getCompICa)/len(getCompICa),2)))
  plt.title('Total Time to Assemble 50 Powerboards with assembleComponents.py')
  plt.xlabel('Time [s]')
  plt.legend()
  plt.show()
  plt.savefig("data/assembleComponents.png")
  plt.close()

  bins = np.histogram(np.hstack((bpoll, bpollI, bpollICa)), bins = 'auto')[1]
  plt.hist(x=bpoll, bins=bins, alpha=0.5, label = 'production_database_scripts')
  plt.hist(x=bpollI, bins=bins, alpha=0.5, label = 'itkdb')
  plt.hist(x=bpollICa, bins=bins, alpha=0.5, label = 'itkdb Cached')
  plt.title('listComponents call for bPOL12V')
  plt.xlabel('Time [s]')
  plt.legend()
  plt.show()
  plt.savefig("data/assembleComponents_bpList.png")
  plt.close()

  bins = np.histogram(np.hstack((bpolg, bpolgI, bpolgICa)), bins = 'auto')[1]
  plt.hist(x=bpolg, bins=bins, alpha=0.5, label = 'production_database_scripts')
  plt.hist(x=bpolgI, bins=bins, alpha=0.5, label = 'itkdb')
  plt.hist(x=bpolgICa, bins=bins, alpha=0.5, label = 'itkdb Cached')
  plt.title('getComponents call for bPOL12V')
  plt.xlabel('Time [s]')
  plt.legend()
  plt.show()
  plt.savefig("data/assembleComponents_bpGet.png")
  plt.close()

  bins = np.histogram(np.hstack((hvl, hvlI, hvlICa)), bins = 'auto')[1]
  plt.hist(x=hvl, bins=bins, alpha=0.5, label = 'production_database_scripts')
  plt.hist(x=hvlI, bins=bins, alpha=0.5, label = 'itkdb')
  plt.hist(x=hvlICa, bins=bins, alpha=0.5, label = 'itkdb Cached')
  plt.title('listComponents call for HVMUX')
  plt.xlabel('Time [s]')
  plt.legend()
  plt.show()
  plt.savefig("data/assembleComponents_hvList.png")
  plt.close()

  bins = np.histogram(np.hstack((hvg, hvgI, hvgICa)), bins = 'auto')[1]
  plt.hist(x=hvg, bins=bins, alpha=0.5, label = 'production_database_scripts')
  plt.hist(x=hvgI, bins=bins, alpha=0.5, label = 'itkdb')
  plt.hist(x=hvgICa, bins=bins, alpha=0.5, label = 'itkdb Cached')
  plt.title('getComponents call for HVMUX')
  plt.xlabel('Time [s]')
  plt.legend()
  plt.show()
  plt.savefig("data/assembleComponents_hvGet.png")
  plt.close()

  bins = np.histogram(np.hstack((lpl, lplI, lplICa)), bins = 'auto')[1]
  plt.hist(x=lpl, bins=bins, alpha=0.5, label = 'production_database_scripts')
  plt.hist(x=lplI, bins=bins, alpha=0.5, label = 'itkdb')
  plt.hist(x=lplICa, bins=bins, alpha=0.5, label = 'itkdb Cached')
  plt.title('listComponents call for PWB_LINPOL')
  plt.xlabel('Time [s]')
  plt.legend()
  plt.show()
  plt.savefig("data/assembleComponents_lpList.png")
  plt.close()

  bins = np.histogram(np.hstack((lpg, lpgI, lpgICa)), bins = 'auto')[1]
  plt.hist(x=lpg, bins=bins, alpha=0.5, label = 'production_database_scripts')
  plt.hist(x=lpgI, bins=bins, alpha=0.5, label = 'itkdb')
  plt.hist(x=lpgICa, bins=bins, alpha=0.5, label = 'itkdb Cached')
  plt.title('getComponents call for PWB_LINPOL')
  plt.xlabel('Time [s]')
  plt.legend()
  plt.show()
  plt.savefig("data/assembleComponents_lpGet.png")
  plt.close()

  bins = np.histogram(np.hstack((cl, clI, clICa)), bins = 'auto')[1]
  plt.hist(x=cl, bins=bins, alpha=0.5, label = 'production_database_scripts')
  plt.hist(x=clI, bins=bins, alpha=0.5, label = 'itkdb')
  plt.hist(x=clICa, bins=bins, alpha=0.5, label = 'itkdb Cached')
  plt.title('listComponents call for PWB_COIL')
  plt.xlabel('Time [s]')
  plt.legend()
  plt.show()
  plt.savefig("data/assembleComponents_cList.png")
  plt.close()

  bins = np.histogram(np.hstack((cg, cgI, cgICa)), bins = 'auto')[1]
  plt.hist(x=cg, bins=bins, alpha=0.5, label = 'production_database_scripts')
  plt.hist(x=cgI, bins=bins, alpha=0.5, label = 'itkdb')
  plt.hist(x=cgICa, bins=bins, alpha=0.5, label = 'itkdb Cached')
  plt.title('getComponents call for PWB_COIL')
  plt.xlabel('Time [s]')
  plt.legend()
  plt.show()
  plt.savefig("data/assembleComponents_cGet.png")
  plt.close()

  bins = np.histogram(np.hstack((sl, slI, slICa)), bins = 'auto')[1]
  plt.hist(x=sl, bins=bins, alpha=0.5, label = 'production_database_scripts')
  plt.hist(x=slI, bins=bins, alpha=0.5, label = 'itkdb')
  plt.hist(x=slICa, bins=bins, alpha=0.5, label = 'itkdb Cached')
  plt.title('listComponents call for PWB_SHIELDBOX')
  plt.xlabel('Time [s]')
  plt.legend()
  plt.show()
  plt.savefig("data/assembleComponents_sList.png")
  plt.close()

  bins = np.histogram(np.hstack((sg, sgI, sgICa)), bins = 'auto')[1]
  plt.hist(x=sg, bins=bins, alpha=0.5, label = 'production_database_scripts')
  plt.hist(x=sgI, bins=bins, alpha=0.5, label = 'itkdb')
  plt.hist(x=sgICa, bins=bins, alpha=0.5, label = 'itkdb Cached')
  plt.title('getComponents call for PWB_SHIELDBOX')
  plt.xlabel('Time [s]')
  plt.legend()
  plt.show()
  plt.savefig("data/assembleComponents_sGet.png")
  plt.close()

  bins = np.histogram(np.hstack((totl, totlI, totlICa)), bins = 'auto')[1]
  plt.hist(x=totl, bins=bins, alpha=0.5, label = 'production_database_scripts')
  plt.hist(x=totlI, bins=bins, alpha=0.5, label = 'itkdb')
  plt.hist(x=totlICa, bins=bins, alpha=0.5, label = 'itkdb Cached')
  plt.title('listComponents call for All PWB Components')
  plt.xlabel('Time [s]')
  plt.legend()
  plt.show()
  plt.savefig("data/assembleComponents_totList.png")
  plt.close()

  bins = np.histogram(np.hstack((totg, totgI, totgICa)), bins = 'auto')[1]
  plt.hist(x=totg, bins=bins, alpha=0.5, label = 'production_database_scripts: Mean' + str(round(sum(totg)/len(totg),2)))
  plt.hist(x=totgI, bins=bins, alpha=0.5, label = 'itkdb: Mean' + str(round(sum(totgI)/len(totgI),2)))
  plt.hist(x=totgICa, bins=bins, alpha=0.5, label = 'itkdb Cached: Mean' + str(round(sum(totgICa)/len(totgICa),2)))
  plt.title('getComponents call for All PWB Components')
  plt.xlabel('Time [s]')
  plt.legend()
  plt.show()
  plt.savefig("data/assembleComponents_totGet.png")
  plt.close()
