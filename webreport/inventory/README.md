# Installation
The inventory scripts should be installed as part of the `reporting` package using the instructions in the top-level README.

Prepare an area that will host the generated web page. This consists of identifying a directory that will contain the generated page. This assumes that `/var/www/inventory` is used.

For local development, any directory will work.

For a public page, this directory should be hosted by your web server. If using Apache, see the [Mapping URLs to Filesystem Locations](https://httpd.apache.org/docs/trunk/urlmapping.html) section of their documentation

# Generating Inventory Report
Run the following, where `/var/www/inventory` is the directory that will contain the generated scripts. 

```bash
inventory.py /var/www/inventory
```

Note the static assets (CSS, images) inside `static` are copied and will overwrite existing contents.

## Automatic Updates
Add the following line inside your crontab file to automatically regenerate the report every hour.

```crontab
0 * * * * source /var/www/reporting/venv/bin/activate && ITKDB_ACCESS_CODE1=your-code-1 ITKDB_ACCESS_CODE2=your-code-2 inventory.py /var/www/inventory
```
Don't forget to replace `/var/www/reporting/venv` with the path to your virtual environment.