
class StringParserAndErrorDetection:
    """
    Parse content of string inside two key words in the following format:

    key1 content key2

    After the parsing, the class will store all the unique contents in a list, and count how many contents (regardless of duplicated or not) show up.

    The class will also do error and warning detection (with keyword error and warning showing up in any line).

    """
    def __init__(self,start,end):
        self.contents = [] # unique contents that show up
        self.nContents = 0 # number of lines that has the two key words, regardless of duplicated contents or not
        self.hasWarning = False
        self.hasError = False
        self.key1 = start
        self.key2 = end

    def clear(self):
        self.contents = []
        self.nContents = 0
        self.hasWarning = False
        self.hasError = False

    def parse(self,line):
        if "warning" in line.lower():
            self.hasWarning = True
        if "error" in line.lower() or "fail" in line.lower():
            self.hasError = True
        idx_key1 = line.find(self.key1)
        idx_key2 = line.find(self.key2)
        if idx_key1 == -1 or idx_key2 == -1:
            return False

        self.nContents += 1 
        content = line[idx_key1+len(self.key1):idx_key2]
        if content not in self.contents:
            self.contents.append(content)
         
        return True
