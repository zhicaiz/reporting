import os
import re
import threading
import glob
import sys
from io import StringIO 

from . import procmon
from . import streamparser
from . import pbv3parser
from . import stringparser_errordetection

from database.pbv3_upload_tests import upload_test_result_file

class Capturing(list):
    def __enter__(self):
        self._stdout = sys.stdout
        sys.stdout = self._stringio = StringIO()
        return self
    def __exit__(self, *args):
        self.extend(self._stringio.getvalue().splitlines())
        del self._stringio    # free up some memory
        sys.stdout = self._stdout

class ICommandRunner:
    """
    Controls running and parsing the test results of a single panel.

    This is an interface class. It should not be used directly.

    The running of a test is done by executing a command and parsing the
    output. The command is build by the `command` function that subclasses
    classes have to implement.
    
    Thread safe

    Parameters:
     datadir (str): Command will be executed in this directory.
     datadir_local (str): if running via ssh, this is the directory of test results on the local PC (not ZTurn) - used for data uploading purpose
    """
    def __init__(self, datadir='.', datadir_local=None):
        self.datadir=datadir
        self.datadir_local=datadir_local

        # Process data
        self.proc=None
        self.proc_func=False
        self.data=[]
        self.lock=threading.Lock()

        # Parsing helpers
        self.reaesc = re.compile(r'\x1b[^m]*m')        
        self.sparser = streamparser.StreamParser()
        self.dbparser = streamparser.StreamParser()

        self.ofin=pbv3parser.OFParser()
        self.padi=pbv3parser.SingleValueParser('PADID')
        self.bert=pbv3parser.SingleValueParser('BER')
        self.temp=pbv3parser.SingleValueParser('TEMPERATURE')
        self.dcad=pbv3parser.SingleValueParser('DCDC_ADJUST', prefix='--->')
        self.lven=pbv3parser.LVEnParser()
        self.hven=pbv3parser.HVEnParser()
        self.tout=pbv3parser.ToggleOutputParser()
        self.lviv=pbv3parser.LVIVParser()
        self.rshx=pbv3parser.DACParser('RAMPDACShuntx')
        self.rshy=pbv3parser.DACParser('RAMPDACShunty')
        self.rcax=pbv3parser.DACParser('RAMPDACCalx')
        self.rcay=pbv3parser.DACParser('RAMPDACCaly')
        self.dcdc=pbv3parser.DCDCEffParser()
        self.amsl=pbv3parser.AMSlopeParser()
        self.hvrt=pbv3parser.HVScanParser()
        self.dbUploadString=stringparser_errordetection.StringParserAndErrorDetection(start='===> UPLOADING FINISHED for ', end=' <===')

        self.sparser.parsers.append(self.tout)
        self.sparser.parsers.append(self.ofin)
        self.sparser.parsers.append(self.padi)
        self.sparser.parsers.append(self.bert)
        self.sparser.parsers.append(self.temp)
        self.sparser.parsers.append(self.dcad)
        self.sparser.parsers.append(self.lven)
        self.sparser.parsers.append(self.hven)
        self.sparser.parsers.append(self.lviv)
        self.sparser.parsers.append(self.rshx)
        self.sparser.parsers.append(self.rshy)
        self.sparser.parsers.append(self.rcax)
        self.sparser.parsers.append(self.rcay)
        self.sparser.parsers.append(self.dcdc)
        self.sparser.parsers.append(self.amsl)
        self.sparser.parsers.append(self.hvrt)
        self.dbparser.parsers.append(self.dbUploadString)

    @property
    def running(self):
        if self.proc_func:
            return True
        if self.proc==None:
            return False
        return self.proc.is_alive()

    def stop(self):
        """ Terminate the current running program """
        if self.proc_func:
            self.proc_func=False
        if not self.running:
            return

        self.proc.terminate()
        self.proc.join()

    def runGeneral(self, program='pbv3_mass_test', option=None, useConfig=True):
        self.lock.acquire(blocking=True) # Make sure we are the only runners

        if self.running:
            self.lock.release()
            return

        #
        # Start the run command
        self.data=[]
        for parser in self.sparser.parsers:
            parser.clear()

        cmd=self.commandGeneral(program, option, useConfig)
        if self.ip!=None:
            self.proc = procmon.ProcessMonitor(cmd)
        else:
            self.proc = procmon.ProcessMonitor(cmd, cwd=self.datadir)
        self.proc.start()
        self.lock.release()

    def run(self, version, batch, boards, serials, panelName, institution=None, debug=0, basic=False):
        self.lock.acquire(blocking=True) # Make sure we are the only runners

        if self.running:
            self.lock.release()
            return

        #
        # Start the run command
        self.data=[]
        for parser in self.sparser.parsers:
            parser.clear()

        cmd=self.command(version, batch, boards, serials, panelName, institution, debug, basic)
        if self.ip!=None:
            self.proc = procmon.ProcessMonitor(cmd)
        else:
            self.proc = procmon.ProcessMonitor(cmd, cwd=self.datadir)
        self.proc.start()
        self.lock.release()

    def runUploadData(self, client, version, batch, boards, serials, panelName):
        self.lock.acquire(blocking=True) # Make sure we are the only runners

        if self.running:
            self.lock.release()
            return

        #
        # Start the run command
        self.data=[]
        for parser in self.dbparser.parsers:
            parser.clear()

        result_dir = self.datadir
        if self.datadir_local != None:
            result_dir = self.datadir_local
        list_boards = list(serials)
        result_dir += "/panel"+panelName

        """
        Imagine we did three test runs for a panel, at time1, time2, and time3
        then in the result directory, we will see three sub-directories: time1, time2, time3
        And imagine at time1 we tested pb1, at time2 we tested pb2, and at time3 we tested pb1 again for some reason (for example fixing a wirebond)
        Then we will see those directories:
        time1/pb1
        time2/pb2
        time3/pb1

        The goal is to upload all LATEST test results for all powerboards selected in this panel, so we need to upload:
        time2/pb2
        time3/pb1
        """
        subdirs = glob.glob(result_dir+"/*")

        if len(subdirs) == 0:
            self.lock.release()
            return "No file to upload!"

        nFiles = 0
        self.proc_func=True
        try:
            for board in list_boards:
                for fileType in ["alive.json", "functionality.json", "advanced.json"]:
                    filesAll = glob.glob(result_dir+"/*/pb20USBP0{:01d}{:02d}{:04d}".format(version, batch, int(board))+"/*"+fileType)
                    filesAll_sorted = sorted(filesAll, key = os.path.getmtime)
                    if len(filesAll_sorted) == 0:
                        continue
                    file_json = filesAll_sorted[-1] # only upload the LATEST file
                    if os.path.getsize(file_json) < 10: #ignore empty file
                        continue
                    nFiles += 1
                    with Capturing() as output:
                        upload_test_result_file(file_json, client)
                    self.parseOutput(output)
        except Exception:
            print("Get some exception in uploading files...")

        self.proc_func=False

        if nFiles == 0:
            self.lock.release()
            return "No file to upload!"
            
        self.lock.release()
        return ""

    def parse(self):
        if self.proc==None: return
            
        if not self.lock.acquire(blocking=False):
            return # Someone else will parse...

        while self.proc.queue.qsize()>0:
            line=self.proc.queue.get(block=False)
            print(line)
            line=self.reaesc.sub('',line)
            self.data.append(line)
            self.sparser.parse(line)

        self.lock.release()

    def parseOutput(self, output):
        for line in output:
            print(line)
            line=self.reaesc.sub('',line)
            self.data.append(line)
            self.dbparser.parse(line)

    def commandGeneral(self, program='pbv3_mass_test', option=None, useConfig=True):
        """
        Command to execute. All inheriting classes need to implement this.

        The return value should be the command formatted in a way that can be
        passed to `procmod.ProcessMonitor`.

        Parameters:
         program (str): program to execute 
         option (list of str): option to be added to the program, for example, ['-b', '1', '--advanced']
         useConfig (bool): if true, "-e path/to/equipconfig.json" will be appended to command
        """
        raise Exception('Runner must implement `commandGeneral`!')

    def command(self, version, batch, boards, serials, panelName, institution=None, debug=0, basic=False):
        """
        Command to execute to run Powerboard QC. All inheriting classes need to implement this.

        The return value should be the command formatted in a way that can be
        passed to `procmod.ProcessMonitor`.

        Parameters:
         version (int): powerboard version id
         batch (int): powerboard batch id
         boards (list of int): list of powerboard positions to test
         serials (list of int): list of powerboard serials (last four digits) at each position
         panelName (str): name of panel
         institution (str): institution where the test is performed
         debug (int): debug level, 0 means none
         basic (bool): run basic tests only
        """
        raise Exception('Runner must implement `command`!')
        
class PowertoolsRunner(ICommandRunner):
    """
    Executes the `pbv3_mass_test` QC test program from the powertools
    package.
    """
    def __init__(self, datadir='.', datadir_local=None, equipconf='', ip=None, powertools='/usr', hbipc=False):
        """
        Parameters:
         equipconf (str): location of the hardware configuration file
         ip (str): IP address on which powertools should be executed, None if run locally
         datadir (str): See `ICommandRunner`
         powertools (str): Path to powertools installation.
        """
        super().__init__(datadir=datadir)

        # Store configuration
        self.datadir_local=datadir_local
        self.equipconf=equipconf
        self.ip=ip
        self.powertools=powertools
        self.hbipc=hbipc

    def commandGeneral(self, program='pbv3_mass_test', option=None, useConfig=True):
        cmd=[]
        if self.ip!=None:
            cmd+=['ssh']
            cmd+=[self.ip]
            cmd+=['cd '+self.datadir+'; ']
        cmd+=['{prefix}/bin/{exe}'.format(prefix=self.powertools, exe=program)]
        if option != None:
            cmd+=option

        if useConfig:
            cmd+=['-e', self.equipconf]

        return cmd

    def command(self, version, batch, boards, serials, panelName, institution=None, debug=0, basic=False):
        options=[]

        # Run settings
        if debug>0:
            options+=['-'+'d'*debug]

        # Enable hybrid burn-in powerboard carrier test sequence.
        if self.hbipc:
            options+=["--hbipc"]

        if not basic:
            options.append('--advanced')

        # Powerboard identification options
        if institution is not None and institution!="":
            options+=['--institution', institution]
        options+=['--version', str(version)]
        options+=['--batch'  , str(batch)]
        options+=['--board']
        options+=[','.join(map(str,boards ))]
        options+=['--serial']
        options+=[','.join(map(str,serials))]
        options+=[panelName]

        return self.commandGeneral(program='pbv3_mass_test', option=options, useConfig=True)

class DummyRunner(ICommandRunner):
    """
    Print the contents of a file.

    The file should be the result of a previous run of `pbv3_mass_test`.
    """
    def __init__(self, logpath, datadir='.'):
        """
        Parameters:
         logpath (str): Print contents of file at this location.
         datadir (str): See `ICommandRunner`
        """
        super().__init__(datadir=datadir)

        # Store configuration
        self.logpath=logpath

    def command(self, version, batch, boards, serials, panelName, debug=0, basic=False):
        cmd=['cat',self.logpath]

        return cmd
